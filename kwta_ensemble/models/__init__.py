# k-Winners-Take-All Ensemble Neural Network
# Copyright (C) 2021  Abien Fred Agarap
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from kwta_ensemble.models.cnn import CNN
from kwta_ensemble.models.dnn import DNN
from kwta_ensemble.models.ensemble import Ensemble
from kwta_ensemble.models.kwta_ensemble import kWTAEnsemble
from kwta_ensemble.models.lenet import LeNet
from kwta_ensemble.models.prefex_net import PrefexDNN
